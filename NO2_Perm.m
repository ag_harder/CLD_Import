% NO2 Emissionsrate
em_NO2_M=42e-9; % g NO2 / Min
M_mol_NO2=16+16+14;
avog=6.022e23;

em_NO2_mol=em_NO2_M/M_mol_NO2; % mol NO2/min
em_NO2_molec=em_NO2_mol*avog; 

Vol_Raum=65*100*100*100; % Raumvolumen, DIeters Büro in cc
LoSchmidt=2.69e19;
x=1:(3*1440);
NO2_MixRat=em_NO2_molec.*x./Vol_Raum./LoSchmidt;
