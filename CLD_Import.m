%% Script to import and plot data from the NOx CLD

% Path and Filename definitions
DataPath='\\osiris\lab\CLD';
FileName='CR3000_NOx_RawData.dat';

% Cal factors 22.10.2019
cal.NO_cps_slope=2409.92 % range of Sensitivity=2392.3 - 2424.5 cps/ppb
cal.NO_cps_offset=1.29
cal.eff_BLC=0.1989;

%% Read data 
CLD_NOx=CLD_ReadCSV(fullfile(DataPath,FileName));

%% calculate NO & NO2 from data
CLD_NOx.NO_cps=CLD_NOx.NO_main-CLD_NOx.NO_pre;
CLD_NOx.NOc_cps=CLD_NOx.NOc_main-CLD_NOx.NOc_pre;

CLD_NOx.NO_ppb=(CLD_NOx.NO_cps-cal.NO_cps_offset)./cal.NO_cps_slope;
CLD_NOx.NOc_ppb=(CLD_NOx.NOc_cps-cal.NO_cps_offset)./cal.NO_cps_slope;
CLD_NOx.NO2_ppb=(CLD_NOx.NOc_ppb-CLD_NOx.NO_ppb)./cal.eff_BLC;

%%
stdfigure
area([datetime(2021,2,5,12,00,0),datetime(2021,2,5,12,56,0)],[30,30],'facecolor',[.8,1,.8],'facealpha',.5,'edgecolor','none', 'basevalue',-10);
hold on;

area([datetime(2021,2,5,13,25,0),datetime(2021,2,5,14,17,0)],[30,30],'facecolor',[.8,1,.8],'facealpha',.5,'edgecolor','none', 'basevalue',-10);
area([datetime(2021,2,5,14,43,0),datetime(2021,2,5,15,09,0)],[30,30],'facecolor',[.8,1,.8],'facealpha',.5,'edgecolor','none', 'basevalue',-10);
plot(CLD_NOx.TIMESTAMP,CLD_NOx.NO_ppb,'b.-', 'MarkerSize',20);
plot(CLD_NOx.TIMESTAMP,CLD_NOx.NO2_ppb,'r.-', 'MarkerSize',20);
xlabel('Zeit local [h]');
ylabel('Mischungsverhältnis [ppbv]');
xlim([datetime(2021,2,5,11,35,0) datetime(2021,2,5,15,10,0)  ])


legend('YouVee Lamp on','YouVee Lamp on','YouVee Lamp on','NO','NO_2','Location','northwest');
grid on
hold off;
%
stdfigure
area([datetime(2021,2,5,18,06,0),datetime(2021,2,5,22,0,0)],[90,90],'facecolor',[.8,1,.8],'facealpha',.5,'edgecolor','none', 'basevalue',-10);
hold on;
plot(CLD_NOx.TIMESTAMP,CLD_NOx.NO_ppb,'b.-', 'MarkerSize',20);
hold on;
plot(CLD_NOx.TIMESTAMP,CLD_NOx.NO2_ppb,'r.-', 'MarkerSize',20);
xlabel('Zeit local [h]');
ylabel('Mischungsverhältnis [ppbv]');
xlim([datetime(2021,2,5,17,20,0) datetime(2021,2,5,22,0,0)  ])

legend('YouVee Lamp on','NO','NO_2','Location','northeast');
grid on
hold off;
%


