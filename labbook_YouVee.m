%% Flags:
% 0  ->  "standard" measurement with IPI (PropeneSwitching) 
% -> usually not to be set explicitly!
% 1  ->  first measurements with IPI (C3F6Switching)
% 2  ->  standard measurement without IPI
% 3  ->  measurement with IPI (NO Switching!)
% 4  ->  calibration without IPI
% 5  ->  IPI on/off test 
% 6  ->  test IPI with Hg lamp
% 7  ->  test with IPI 2nd generation (plastic bottle) 
% 8  ->  ShowerTests 
% 9  ->  
% 10 ->  invalid data - maintenance


%% (relevant)lab book entries for julian day 207
% -> in the form labbok.jdXXX={starttime, stoptime,flag,comment};


lb={
    '07:54','08:00',10,'took IPI off';
    '08:00','08:50', 2,'measurement IPI off';
    '08:50','08:53',10,'setup IPI again';
    '09:29','09:31',10,'took IPI off';
    '09:31','10:00', 2,'measurement IPI off';
    '10:00','10:05',10,'setup IPI';
    '10:09','11:21', 6,'test with Hg lamp on top of IPI';
    '11:21','11:54', 2,'measurement IPI off -->check MFC NO! (broken?!?)';
    '11:54','14:15', 6,'test with Hg lamp on top of IPI/Calibrator';
    '16:06','17:10',10,'MFC NO got exchanged';
    }
    
eval(['labbook.jd',num2str(jd),'=lb']);
clear lb;