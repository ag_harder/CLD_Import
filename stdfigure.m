
fontsizevalue=22;

figure
% orient landscape
% 
% % set(0,'defaultaxesunit','centimeters')
% % set(0,'defaultaxesposition',[4 3 24 16])
% % 
% % set(gcf,'Units','centimeter')
% % set(gcf,'PaperPosition',[0 0 29.7 21])
% % 
% % set(gcf,'Position',[1 1 29 20])

set(findobj('LineWidth',0.5),'LineWidth',1)
set(gca,'FontSize',fontsizevalue,'FontWeight','Bold')
set(get(gca,'XLabel'),'FontSize',fontsizevalue,'FontWeight','Bold')
set(get(gca,'YLabel'),'FontSize',fontsizevalue,'FontWeight','Bold')
set(get(gca,'Title'),'FontSize',fontsizevalue,'FontWeight','Bold')
